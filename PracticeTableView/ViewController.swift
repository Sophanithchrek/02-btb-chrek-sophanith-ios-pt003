//
//  ViewController.swift
//  PracticeTableView
//
//  Created by SOPHANITH CHREK on 17/11/20.
//

import UIKit

class ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var previewTableView: UITableView!
    
    // Declare array to store the data for insert into UITableViewCell
    var footballClub = [
        ["Juventus", "Dybala", "C.Rolnaldo", "Danilo"],
        ["Real Madrid", "Hazard", "Asensio", "Isco"],
        ["Chelsea", "Havert", "Werner", "Kovasic"]
    ]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        // Set delegate for UITableViewDataSource
        previewTableView.dataSource = self
        previewTableView.delegate = self
        
        // Configure xib file to show in UITableView
//        previewTableView.register(PreviewTableViewCell.self, forCellReuseIdentifier: "myCell")
        previewTableView.register(UINib(nibName: "PreviewTableViewCell", bundle: nil), forCellReuseIdentifier: "myCell")
    }
    
    // Implement section for UITableView
    func numberOfSections(in tableView: UITableView) -> Int {
        return footballClub.count
    }
    
    // Implement numberOfRowsInSections for UITableView
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return footballClub[section].count
    }

    // Implement cellForRowAt for UITableView
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let viewCell = previewTableView.dequeueReusableCell(withIdentifier: "myCell", for: indexPath)
        
        // Get data from model and add it into UITableView
        viewCell.textLabel?.text = footballClub[indexPath.section][(indexPath).row]

        return viewCell
    }
    
    // Implement header for UITableView
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return footballClub[section][0]
    }
    
    // Implement to add action to each row of UITableView
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            footballClub[indexPath.section].remove(at: indexPath.row)
            tableView.reloadData()
        }
    }

}

